import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Player{

  public String tokenColor;
  private BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

  public Player() {
    System.out.println("Your color is red. It will be denoted by r in the board!");

  }

  public int getUserInput(){
    //Function to get user getUserInput
    String toReturn = null;
    int colNum=0;

    while (colNum == 0){
      System.out.println("Please enter the column to insert your token");
    
		
		try{	
      //try block to guard against character/whitespace and invalid inputs		
			toReturn = input.readLine();
      colNum= Integer.parseInt(toReturn);
      //If column number entered is greater than 7, print that this is invalid input
      if (colNum > 7 | colNum < 1) { 
      System.out.println ("Invalid Input!");
      colNum=0;
      }
		}catch(Exception e){
      System.out.println("Invalid input!");
			toReturn = null;
		}
    }
    return colNum;
	}
  
}