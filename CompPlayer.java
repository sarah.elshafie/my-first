import java.util.*;

public class CompPlayer {

  Random rand = new Random();
  String color;
  char colorSymbol;

  public CompPlayer(String color){
    System.out.println("Computer Player created! My color is " + color + " and denoted by "+ getColorSymbol(color));
    this.color=color;
    this.colorSymbol=getColorSymbol(color);
  }

  public int getCompInput(){
    int i = rand.nextInt(7);
    return i;

  }
 
 public char getColorSymbol(){
   //Accessor function to return color
   return colorSymbol;
 }

 public String returnColor(){
   //Acccessor to return player color
   return color;
 }






 public char getColorSymbol (String color){
   //Symbol corresponding to the color. Multiple colors to allow multiple computer players.
   String [][] colorMatrix = { { "yellow", "y"}, {"blue","b"},{"Magenta","m"} };
   for (int i=0; i < 2;++i){
     if (colorMatrix[i][0]==color){
       return colorMatrix[1][i].charAt(0);
     }
   }
   return 'g';

 } 

}